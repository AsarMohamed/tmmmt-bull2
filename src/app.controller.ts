import {Body, Controller, Get,Module, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';

import {DoneCallback, Job, Queue} from 'bull';
import {BullModule, InjectQueue} from 'nest-bull';
// @Controller()
// export class AppController {
//   constructor(private readonly appService: AppService) {}

//   @Get()
//   getHello(): string {
//     return this.appService.getHello();
//   }
// }


 
@Controller()
export class AppController {
 
  constructor(
    @InjectQueue('server A') readonly queue: Queue,
  ) {}
 
  @Get()
  async addJob( @Body() value: any ) {
    let arr = [1, 2, 3, 4,5,6,7,8,9,10];
    // console.log('1111111111111',this.queue)
    for (var val of arr) {
      const job: Job = await this.queue.add('print', val);
      // console.log('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj',job.id)
    }
    return true;
  }
}
//bull setup
// var test = new Queue('test');

// //arena setup
// const arena = Arena({
//   queues: [{
//     "name": "test",
//     "port": 6381,
//     "host": "127.0.0.1",
//     "hostId": "ServerName"
//   }]
// );
// app.use('/arena', arena);