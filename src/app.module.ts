// import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppProcessor } from './processor';
import {DoneCallback, Job, Queue} from 'bull';
// import { join } from 'path';

import { Module, OnModuleInit, Logger } from '@nestjs/common';

import { BullModule, InjectQueue } from 'nest-bull';
import { join } from 'path';
 
@Module({
  imports: [
    BullModule.register({
      name: 'server A',
      options: {
        redis: {
          port: 6379,
        },
      },
      // processors: [ "/home/tmmmt/tmmmt-bull2/src/processor.ts" ]
    }) 
  ],
  controllers: [
        AppController,
      ],
  providers: [AppService, AppProcessor],
})
export class AppModule {

  private readonly logger = new Logger(AppModule.name, true);
  constructor(
    @InjectQueue('server A') readonly queue: Queue,
  ) { }

  onModuleInit() {

    this.queue.add('print', { currency: 'USD2' }, {
      repeat: { cron: '*/2 * * * *'},
      // removeOnFail: false,
      removeOnComplete: true,
      jobId: 'print-scheduler'}).then(job => {
      this.logger.log(`Job ${job.name} successfully started and will run every minute`)
    });

    this.queue.on('completed', (job, result) => {
      this.logger.log(`Job ${job.name} completed! Result: ${result}`);
    });

    this.queue.on('failed', (job, err) => {
      this.logger.error(err)
    })

  }


}


// curl -XGET 'elasticsearch-domain-endpoint/_snapshot/_status'


// cd /etc/logrotate.d
// sudo vi tmmmtlog

// /home/ubuntu/tmt-node-apps/tmmmt-apis.log.* {
//   weekly
//   rotate 8
//   notifempty
//   compress
//   olddir /var/log/tmtlogs
//   dateext
//   dateformat -%Y-%m-%d.log
//   nocreate
  
//   }
// cd /var/log/
// sudo mkdir tmtlogs

// sudo logrotate -f /etc/logrotate.conf
