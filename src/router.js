const Arena = require('bull-arena');
const express = require('express');

const app = express();

const arenaConfig = Arena({
    queues: [{
        name: "Server A",
        hostId: "MyAwesomeQueues",
        redis: {
            port: 6379,
            host: "localhost"
        },
    }, ],
}, {
    basePath: '/arena',
    disableListen: true
});

app.use('/', arenaConfig);
const PORT = 1231;
app.listen(PORT, function() {
    console.log("running on port ", PORT)
})