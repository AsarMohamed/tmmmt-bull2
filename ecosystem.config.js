module.exports = {
    apps: [{
        name: 'API',
        script: 'dist/main.js',

        // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
        args: 'one two',
        instances: 2,
        autorestart: true,
        exec_mode: "cluster",
        watch: false,
        max_memory_restart: '1G',
        env: {
            PORT: 3000,
            NODE_ENV: 'development'
        },
        // env_production: {
        //     NODE_ENV: 'production'
        // }
    }],

    // deploy: {
    //     production: {
    //         user: 'node',
    //         host: 'localhost',
    //         ref: 'origin/master',
    //         repo: 'git@github.com:repo.git',
    //         path: '/var/www/production',
    //         'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    //     }
    // }
};